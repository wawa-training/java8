package com.java8.weblog;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestWebLogReader {

    // private static String FILE_LOC="C://shahdab//WAWA//doc//test.log";
    private static String FILE_LOC = "src/main/resources/samplefile/test.log";


    public static void main(String[] args) {
        System.out.println("########Started##########");


        try {
            List<String> list = new ArrayList<String>();
            List<Integer> timeList = new ArrayList<>();
            Stream<String> stream = Files.lines(Paths.get(FILE_LOC));
           TestWebLogReader.getAllByHour(stream);
           TestWebLogReader.getHitCountByHour("00:00:14","01:05:32",stream);

        } catch (Exception e) {

        }
        System.out.println("#########Finished#########");
    }

    private static void getAllByHour(Stream<String> stringStream) {
        System.out.println("########getAllByHour###########");
        List<String> lineList = getTimeList(stringStream);
        List<Integer> timeList = new ArrayList<>();
        Map<Integer, Integer> result = null;
        if (lineList != null && !lineList.isEmpty()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
            timeList = lineList.stream().map(s -> LocalTime.parse(s, formatter).getHour()).collect(Collectors.toList());
            System.out.println("Hours count:" + timeList.size());
            //timeList.forEach(System.out::println);
             result = timeList.stream().collect(Collectors.toMap(Function.identity(), i -> 1 * 1, (i1, i2) -> i1 + i2));

        }
        result.forEach((k, v) -> System.out.println(("No. of hit between " + k + " - " + (k + 1) + " =" + v)));
    }

    private static void getHitCountByHour(String startHour, String endHour, Stream<String> stringStream) {

        System.out.println("#########getHitCountByHour#########");
        List<String> lineList = getTimeList(stringStream);
        List<Integer> hourList = new ArrayList<>();
        if (lineList != null && !lineList.isEmpty()) {
            System.out.println("Lines count:" + lineList.size());
            //list.forEach(System.out::println);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalTime startTime = LocalTime.parse(startHour, formatter);
            LocalTime endTime = LocalTime.parse(endHour, formatter);
            hourList=lineList.stream().map(s -> {
                LocalTime localTime = LocalTime.parse(s, formatter);
                if (localTime.isAfter(startTime) && localTime.isBefore(endTime))
                    return LocalTime.parse(s, formatter).getHour();
                return null;
            }).filter(x -> x!=null).collect(Collectors.toList());
        }
        System.out.println("## No. hit between "+startHour+" - "+endHour+" ="+hourList.size());
    }

    private static List<String> getTimeList(Stream<String> stringStream) {


        List<String> list = new ArrayList<String>();
        try {

            List<Integer> timeList = new ArrayList<>();
            Stream<String> stream = Files.lines(Paths.get(FILE_LOC));
            //stream.forEach(System.out::println);
            list = stream.filter(line -> line.startsWith("Jul")).map(line -> {
                String[] str = line.split(",");
                String[] str1 = str[1].split(" ");
                return str1[2];
            }).collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Number of Line:"+list.size());
        return list;
    }

}
